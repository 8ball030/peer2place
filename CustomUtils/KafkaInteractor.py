from .CONFIG import kafka
from pykafka import KafkaClient
from pykafka.common import OffsetType
from kafka import KafkaConsumer

def getConsumer(topic, group="1"):
    c = KafkaClient(hosts=kafka['hosts'])
    t = c.topics[topic]
    consumer = t.get_simple_consumer(consumer_group=topic + group,
                                     use_rdkafka=True,
                                     queued_max_messages=10**3,
                                     auto_commit_enable=True,
                                     auto_offset_reset=OffsetType.LATEST)
    return consumer

def getConsumer2(topic, group):
    consumer = KafkaConsumer(topic, group_id=group,
                             bootstrap_servers=kafka['hosts'])
  #  c = KafkaClient(hosts=kafka['hosts'])
  #  t = c.topics[topic]
  #  consumer = t.get_simple_consumer(consumer_group=topic + group,
  #                                   use_rdkafka=True,
  #                                   queued_max_messages=10**3,
  #                                   auto_commit_enable=True,
  #                                   auto_offset_reset=OffsetType.LATEST)
    return consumer

def getProducer(topic):
    c = KafkaClient(hosts=kafka['hosts'],
                    # use_greenlets=True
                    )
    t = c.topics[topic]
    producer = t.get_producer(
        use_rdkafka=True,
        linger_ms=1)
    return producer
