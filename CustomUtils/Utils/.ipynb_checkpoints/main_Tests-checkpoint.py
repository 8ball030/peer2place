"""
This is the "Main Test Runner" module.

The main application will launch the tests of the files added to the init function

>>> t = Tester()
Initialised!
Tests Loaded!
>>> print(t.runAll())
All Ok!
"""
import sys
import os
import doctest
from unittest import TestCase
import subprocess

class Tester():
    """
    
    class to handle loading and running tests
    
    >>> t = Tester()
    Initialised!
    Tests Loaded!
    >>> print(len(t.tests) >= 0)
    True
    """
    
    def __init__(self):
        """
        Paths added to the test files will be scanned added executed.
        """
        print("Initialised!")
        self.tests = [os.getcwd() + "/CustomUtils/Utils/setup_paths.py",
                      os.getcwd() +"/CustomUtils/Utils/structuredAccountSetup.py",
                      os.getcwd() +"/basic_rule_agent.py",  
                     ]
        print("Tests Loaded!")
    
    def runAll(self):
        """Runs tests for all modules attached to path.

        >>> t = Tester()
        Initialised!
        Tests Loaded!
        >>> print(t.runAll())
        All Ok!
        """    
        for test in self.tests:
            test_path = test.replace(".py", "_Tests.py")
            self.executeTest(self, test_path)
        return "All Ok!"
    
    @staticmethod
    def executeTest(cls, test_path):
        """
        We execute the tests located in the directories, we expect them to print "All Ok!" at the end of their test runs
        """
        results = str(subprocess.check_output(['python3', test_path]))
        if results == "b''":
            pass
        A = (results.strip("'b").strip("\\n"))
        B = ("All Ok!".replace("\n", ""))
        if A != B:
            print(f"Error! {test_path}")
            
    
def main():
    print("Setting up Tests")
    doctest.testmod()
    print("Finished Main Tests")

    
if __name__ == "__main__":
    main()
