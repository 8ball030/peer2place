# here we need to set up with the current portfolio
#
import hashlib
import json
import sys
import uuid
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, joinedload, raiseload
from sqlalchemy.orm.exc import NoResultFound
import os
import ccxt


import CustomUtils.CONFIG as CONFIG
from CustomUtils import ormDBInteractor
from CustomUtils.dbModels.out import (Asset, Portfolio, Strategy, Trader, Balance,
                              Base, Exchange, Market, Ratio)

dbSesh = ormDBInteractor.setUpSession(os.environ.get("DATABASE"))

def getExchangeCCXT(*, exchangeName):
    """
    Takes an exchange name and returns an exchange model
    """
    availableExchanges = {"binance": ccxt.binance,
                          "coin_base_pro": ccxt.coinbasepro,
                          "binanceSIM": ccxt.binance,
                          "bitfinex": ccxt.bitfinex,
                          "exchangeName": ccxt.binance
                          }
    exchangeConfig = CONFIG.exchangesKeys[exchangeName]
    return availableExchanges[exchangeName](exchangeConfig)


def buildExchange(*, exchangeName, location, feeAmount):
    id = hashlib.md5(str(exchangeName + location + str(feeAmount)
                         ).encode("utf-8")).hexdigest()
    ex = Exchange(id=id,
                  name=exchangeName,
                  location=location,
                  feeAmount=feeAmount)
    try:
        dbSesh.query(Exchange).filter(Exchange.id == id).one()
    except NoResultFound:
        ("Exchange Doesnt Exist Exisited!", ex)
        dbSesh.add(ex)
        dbSesh.commit()
    return ex


def buildAssets(session, exModel, exchangeAPI):
    uniqu = list()
    for market in exchangeAPI.fetch_markets():
        if market['base'] not in uniqu:
            uniqu.append(market['base'])
        if market['quote'] not in uniqu:
            uniqu.append(market['quote'])
    for a in uniqu:
        id = hashlib.md5(str(a + a + exModel.id).encode("utf-8")).hexdigest()
        asset = Asset(id=id, name=a,
                      symbol=a, exchangeId=exModel.id)
        try:
            session.query(Asset).filter(Asset.id == id and
                                        Asset.exchangeId == exModel.id).one()
        except NoResultFound:
            ("Asset Doesnt Exist Exisited!", a)
            session.add(asset)
    session.commit()


def buildMarkets(dbSesh, exModel, exchangeAPI):

    markets = exchangeAPI.fetchMarkets()
    for market in markets:
        (market["base"], exModel.id)
        base = dbSesh.query(Asset).filter(
            Asset.name == market["base"]).filter(
            Asset.exchangeId == exModel.id).one()
        quote = dbSesh.query(Asset).filter(
            Asset.name == market["quote"]).filter(
            Asset.exchangeId == exModel.id).one()
        id = genUid([base.name, quote.name, exModel.id])
        m = Market(id=id,
                   exchangeID=exModel.id,
                   name=market['symbol'],
                   baseAssetID=base.id, counterAssetID=quote.id,
                   minimumTrade=market['limits']['amount']['min'])
        try:
            dbSesh.query(Market).filter(Market.id == id and
                                        Market.exchangeID == exModel.id).one()
        except NoResultFound:
            ("Market Doesnt Exist Exisited!", base, quote)
            dbSesh.add(m)
            dbSesh.commit()


def buildPortfolio(dbSesh, excModel, portfolioName):
    id = genUid([excModel.id, portfolioName])
    portfolio = Portfolio(id=id,
                          exchangeID=excModel.id,
                          startingValue=304,
                          description=portfolioName)
    try:
        portfolio = dbSesh.query(
            Portfolio).filter(Portfolio.id == id and
                              Portfolio.exchangeID == excModel.id).one()
    except NoResultFound:
        ("Portfolio Doesnt Exist Exisited!", portfolioName)
        dbSesh.add(portfolio)
        dbSesh.commit()
    print(id)
    return portfolio


def genUid(list):
    return hashlib.md5(str(" ".join(list)).encode("utf-8")).hexdigest()


def buildStrategy(dbSesh, strategyName):
    params = json.dumps({"ratioThreshold": 0.1,
                         "buyOffset": 0.02,
                         "sellOffset": 0.02,
                         "buyPreference": 0.1,
                         "orderSizeType": "minimum",
                         "orderType": "limit"})

    id = genUid(params)
    S = Strategy(id=id,
                 description=strategyName,
                 paramsJSON=params)
    try:
        S = dbSesh.query(Strategy).filter(Strategy.id == id).one()
    except NoResultFound:
        ("Strategy Doesnt Exist Exisited!", strategyName)
        dbSesh.add(S)
        dbSesh.commit()
    return S


def updateBalances(s, exch, exMod):
    # we are updating for the live trader, s we can query
    for cur, bal in exch.fetchBalance()['total'].items():
        # first we map to an asset
        if cur in ["ADD", "ATD", "MTO"]:
            continue
        try:
            a = s.query(Asset).filter(Asset.name == cur).filter(
                Asset.exchangeId == exMod.id).one()
            a.total = bal
            s.merge(a)
            s.commit()
            ("Balance updated")
        except NoResultFound:
            ("Assets Not FOund!!!!!!!", cur)
            continue


def buildRatios(dbSesh, allowedBal, portfolioModel, exModel):
    for k, v in allowedBal.items():
        (k, v)
        try:
            asset = dbSesh.query(
                Asset).filter(
                    Asset.symbol == k).filter(
                    Asset.exchangeId == exModel.id).one()
        except NoResultFound:
            continue
        id = genUid([asset.symbol, portfolioModel.id])
        try:
            dbSesh.query(
                Ratio).filter(
                    Ratio.id == id).filter(
                    Ratio.portfolioId == portfolioModel.id).one()
        except NoResultFound:
            r = Ratio(id=id,
                      assetId=asset.id,
                      portfolioId=portfolioModel.id,
                      allowedRatio=v)
            ("Ratio Didint Exist!", asset.name)
            dbSesh.add(r)
    dbSesh.commit()

def buildTrader(dbSesh, portfolioModel, exModel, stratModel,
                agentName, params=None):

    print(agentName)# we must tell the trader what portfolio, strategy, and exchange
    # id =  hashlib.md5(portfolioModel.id+stratModel.id).hexdigest()# +agentName).h
                               # stratModel.id,
                               #agentName
    hashed = str(abs(hash(agentName)) % (10 ** 8))
    agentName = "Sim_{}".format(hashed)
    id = hashed

    #:w
    #:id = int(hashlib.md5(str(json.dumps(agentName).decode('utf-8', 'ignore'))).hexdigest(), 16)
    print(id)

   # id = genUid([exModel.id, portfolioModel.id,
   #              stratModel.id, agentName])
    with dbSesh.no_autoflush as db:
        try:
            trader = db.query(Trader).options(
            joinedload('*')).filter(Trader.id == id).one()
        except NoResultFound:
            trader = Trader(id=id,
                            portfolioId=portfolioModel.id,
                            strategyId=stratModel.id,
                            exchangeId=exModel.id,
                            agentName=agentName,
                            params=json.dumps(params))
            db.add(trader)
            print("trader Didint Exist!", trader.id)
            db.commit()
            trader = db.query(Trader).options(
            joinedload('*')).filter(Trader.id == id).one()
    return trader



# def buildBalances(dbSesh, exchangeAPI, portfolioModel, avail, tots):
#     # we are updating for the live trader, s we can query

#     for cur, bal in tots:
#         ("Processing {}".format(cur))
#         # first we map to an asset
#         if cur in ["EON", "ADD", "MEETONE", "ATD", "EOP", "IQ"]:
#             continue
#         try:
#             asset = dbSesh.query(
#                 Asset).filter(Asset.name == cur).filter(
#                 Asset.exchangeId == portfolioModel.exchangeID
#             ).one()
#         except NoResultFound:
#            continue 
#         # make bala id
#         id = genUid([asset.id, portfolioModel.id])
#         with dbSesh.no_autoflush as db:
#             try:
#                 balance = db.query(Balance).filter(Balance.id == id).one()
#                 balance.total = bal
#                 balance.available = avail[cur]
#                 db.merge(balance)
#                 ("Balance merged")
#             except NoResultFound:
#                 bal = Balance(id=id,
#                               assetID=asset.id,
#                               portfolioID=portfolioModel.id,
#                               total=bal,
#                               available=avail[cur])
#                 db.add(bal)
#                 ("Balance Didint Exist!", asset.name)
#             db.commit()

import hashlib
import unicodedata



def setupUpLiveEnviron(exchangeName="coin_base_pro",
                       portfolioName="LivePortfolio",
                       strategyName="acrobat",
                       agentName="LivePortfolio",
                       ):
    RATIOS = {"TUSD": 50,
              "BTC": 50}
    dbSesh = setUpSession()
    exchangeAPI = setUpExchange(exchangeName)
    # now we build a record of the exchange
    print("Set up Exchange")
    exModel = buildExchange(dbSesh, name=exchangeName)
    # now we have our exchange, we can build our assets

    print("Set up Exchange Model")
    buildAssets(dbSesh, exModel, exchangeAPI)
    print("Set up Assets")
    # we build our portfolioID
    portfolioModel = buildPortfolio(dbSesh, exModel, portfolioName)
    print("Set up Portolfio")
    # now we build our markets
    buildMarkets(dbSesh, exModel, exchangeAPI)
    print("Set up Markets")
    # now we build our strategy
    stratModel = buildStrategy(dbSesh, strategyName)
    print("Set up Strategy")
    # now we build our ratios, we will use a common sense approach for now...
    buildRatios(dbSesh, RATIOS, portfolioModel, exModel)
    print("Set up Ratios")
    # now we build our balances ...
    avail = exchangeAPI.fetch_balance()['free']
    tots = exchangeAPI.fetchBalance()['total'].items()
    buildBalances(dbSesh, exchangeAPI, portfolioModel, avail, tots)
    print("Set Up Balances")
    # now we have our ratios, we can (finally) build our trader
    trader = buildTrader(dbSesh, portfolioModel,
                         exModel, stratModel, agentName)
    print("Set up Trader")
    updateBalances(dbSesh, exchangeAPI, exModel)
    print("updated Balances")
    return trader

# def createSimulation(portfolioId,
#                         strategyId,
#                         agentId,
#                         exchangeId,
#                         params,
#                         ):
#     RATIOS = {"TUSD": 50,
#               "BTC": 50}
#     avail = {"TUSD": 6000.,
#              "BTC": 1.}
#     dbSesh = setUpSession()
#     exchangeAPI = setUpExchange(exchangeId)
#     # now we build a record of the exchange
#     exModel = buildExchange(dbSesh, name=(exchangeName))
#     # now we have our exchange, we can build our assets
#     buildAssets(dbSesh, exModel, exchangeAPI)
#     # we build our portfolioID
#     portfolioModel = buildPortfolio(dbSesh, exModel, portfolioName)
#     # now we build our markets
#     buildMarkets(dbSesh, exModel, exchangeAPI)
#     # now we build our strategy
#     stratModel = buildStrategy(dbSesh, strategyName)
#     buildRatios(dbSesh, RATIOS, portfolioModel, exModel)
#     # now we build our balances ...
#     buildBalances(dbSesh, exchangeAPI, portfolioModel, avail, avail.items())
#     # now we have our ratios, we can (finally) build our trader
#     trader = buildTrader(dbSesh, portfolioModel,
#                          exModel, stratModel, agentName)
#     return trader


# def createNewSimulation(portfolioId,
#                      strategyId,
#                      agentId,
#                      exchangeId,
#                      params,
#                      ):
#     """
#     params =
#     RATIOS = {"TUSD": 50,
#               "BTC": 50}
#     avail = {"TUSD": 6000.,
#              "BTC": 1.}

#     """
#     dbSesh = setUpSession()
#     exchangeName = dbSesh.query(Exchange).filter(Exchange.id == exchangeId)
#     exchangeName = exchangeName.one().name

#     strategyName = dbSesh.query(Strategy).filter(Strategy.id == strategyId)
#     strategyName = strategyName.one().description

#     exchangeAPI = setUpExchange(exchangeName)
#     # now we build a record of the exchange
#     exModel = buildExchange(dbSesh, name=(exchangeName))
#     # now we have our exchange, we can build our assets
#     buildAssets(dbSesh, exModel, exchangeAPI)
#     # we build our portfolioID
#     portfolioName = str(uuid.uuid4())
#     portfolioModel = buildPortfolio(dbSesh, exModel, portfolioName)
#     # now we build our markets
#     buildMarkets(dbSesh, exModel, exchangeAPI)
#     # now we build our strateg``y
#     stratModel = buildStrategy(dbSesh, strategyName)
#     buildRatios(dbSesh, params["RATIOS"], portfolioModel, exModel)
#     # now we build our balances ...
#     buildBalances(dbSesh, exchangeAPI, portfolioModel,
#                   params["Available"], params["Available"].items())
#     # now we have our ratios, we can (finally) build our trader
#     agentName = "SimulatedTrader_{}".format(uuid.uuid4())
#     trader = buildTrader(dbSesh, portfolioModel,
#                          exModel, stratModel, agentName, params)
#     return trader


def main():
    # we first set up our exchange
    trader = setupUpLiveEnviron("binance")

if __name__ == '__main__':
    # trader = setupUpLiveEnviron()
    
    main()
