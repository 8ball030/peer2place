
#

"""
Created on Wed Mar 27 00:12:26 2019

@author: tom-rae

"""
import json
import sys
from datetime import datetime
from multiprocessing import Queue

sys.path += ["/app"]

from cryptoExchanges import cryptoExchangeSim, cryptoExchangeLive
from cryptoPortfolios import cryptoPortfolio
from cryptoStrategies import Acrobat
from cryptoTrader import Trader

import CustomUtils.Logger as Logger
import CustomUtils.CONFIG as ignoreAPI
import CustomUtils.ormDBInteractor as ormDBInteractor
import CustomUtils.KafkaInteractor as KafkaInteractor

from Utils.structuredAccountSetup import setupUpLiveEnviron 

from sqlalchemy.orm import contains_eager
from dbModels.out import (Asset, Market,
                          Balance, Order)
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import and_
# we set up our local environment
log = Logger.getLogger()


def syncOrders(s, exch):
    exch.options["warnOnFetchOpenOrdersWithoutSymbol"] = False
    ords = exch.fetch_open_orders()
    for ordd in ords:
        ordJson = exch.fetch_order(id=ordd['id'],
                                   symbol=ordd['symbol'])
        ord = s.query(Order).filter(
            Order.idfromexchange == ordJson['id']).all()
        if ord == []:
            print("Order not in Database", ordJson['id'])
        else:
            print("Order Found on exhange", ordJson['id'])
        # stat = exch.fetch_order_status(
        #    ordd.idfromexchange, symbol=ordd.Market.name)
        ## ordd.idfromexchange = ordd.id
        ## ordd.id = str(uuid.uuid4())
        # if stat == ordd.status:
        #    continue
        # else:
        #    ordd.status = stat
        #    ordd.updatedOn = datetime.now().isoformat()
        #    s.merge(ordd)
    # s.commit()


def updateBalances(dbSesh, exchangeAPI, portfolioModel):
    # we are updating for the live trader, s we can query
    avail = exchangeAPI.fetch_balance()['free']
    for cur, bal in exchangeAPI.fetchBalance()['total'].items():
        # print("Processing {}".format(cur))
        # first we map to an asset
        if cur in ["EON", "ADD", "MEETONE", "PHB",
                   "ATD", "EOP", "IQ", "TFUEL", "ONE", "FTM",
                   "BTCB"]:
            continue
        print("Processing :", cur)
        try:
            asset = dbSesh.query(
                Asset).filter(Asset.name == cur).filter(
                Asset.exchangeId == portfolioModel.exchangeID
            ).one()
        except NoResultFound:
            pass
        # make bala id
        with dbSesh.no_autoflush as db:
            try:
                balance = db.query(Balance).filter(
                    Balance.assetID == asset.id).one()
                balance.total = bal
                balance.available = avail[cur]
                db.merge(balance)
                print("Balance merged")
            except NoResultFound:
                print("Balance Didint Exist!", asset.name)
            db.commit()


def setupEnviron(targetPortfolio="LivePortfolio",
                 targetExchange='binance',
                 stratName="acrobat",
                 agentName="LivePortfolio",
                 streamName="clean_live_binance",
                 ):
    streamObject = KafkaInteractor.getConsumer(topic=streamName)
    session = ormDBInteractor.setUpSession()
    # exchange set up
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchangeConfig['exchangeName'] = targetExchange
    exchangeConfig['stateSession'] = session
    if "SIM" in targetExchange:
        exchange = cryptoExchangeSim.cryptoExchangeSim(exchangeConfig)
    else:
        exchange = cryptoExchangeLive.cryptoExchangeLive(exchangeConfig)

    # portfolio and strategy set up
    portfolio = cryptoPortfolio.portfolioObject(portName=targetPortfolio,
                                                exchange=exchange,
                                                log=log,
                                                stateSession=session)
    strategy = Acrobat.Acrobat(strategyName=stratName,
                               stateSession=session)
    # we pass all of these objects to a trader to interact with
    kwargs = {'log': log,
              'agentName': agentName,
              'strategy': strategy,
              'portfolio': portfolio,
              'exchange': exchange,
              'stateSession': session,
              'traderModel': setupUpLiveEnviron(exchangeName=targetExchange,
                                                portfolioName=agentName,
                                                strategyName=stratName,
                                                agentName=agentName,
                                               )
              }
    # and we finally return
    return Trader.TraderAgent(**kwargs), streamObject


def synchOrdersV2(Agent):
    # this first one liner will clean the stored orders from the internal state
    knownOrders = [(Agent.orderManager._cleanOrders(k), v) for (
        k, v) in Agent.orderManager.currentOrders.items()]
    ordersToCheck = [f[1][0] for
                     f in (filter(lambda x: x[1] != [], knownOrders))]
    for o in ordersToCheck:
        orderStateExchange = Agent.exchange.fetch_order(symbol=o.Market.name,
                                                        id=o.idfromexchange)
        if orderStateExchange['status'] == o.status:
            print("Orders status matches")
        else:
            print("Order Not Matching!")
            Agent.exchange.handleClosureOfOrder(o, datetime.now())


def cleanOrders(Agent):
    # this first one liner will clean the stored orders from the internal state
    timeStamp = datetime.now()
    knownOrders = [(Agent.orderManager._cleanOrders(k), v) for (
        k, v) in Agent.orderManager.currentOrders.items()]
    ordersToCheck = [f[1][0] for
                     f in (filter(lambda x: x[1] != [], knownOrders))]
    for o in ordersToCheck:
        orderStateExchange = Agent.exchange.fetch_order(symbol=o.Market.name,
                                                        id=o.idfromexchange)
        if orderStateExchange['status'] == o.status:
            print("Orders status matches")
            continue
        else:
            print("Order Not Matching!")
            if orderStateExchange['status'] == "closed":
                Agent.exchange.handleClosureOfOrder(o, datetime.now())
            else:
                print(orderStateExchange['status'])


def main():
    Agent, consumer = setupEnviron()
    methods = {"1 : Update stored balance with live balance.": updateBalances,
               "2 : Sync orders": synchOrdersV2,
               "3 : Clean Old Orders": cleanOrders,
               }
    a = [print(k) for k in methods.keys()]
    res = input("Please choose\n")
    if int(res) == 1:
        updateBalances(Agent.stateSession, Agent.exchange,
                       Agent.portfolio.model)
    elif int(res) == 2:
        synchOrdersV2(Agent)
    elif int(res) == 3:
        cleanOrders(Agent)


if __name__ == '__main__':
    main()
