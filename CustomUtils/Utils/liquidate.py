import datetime
import sys
import time
from setup_paths import setup_paths
setup_paths()
import ccxt

try:
    sys.path.append("../CustomUtils")
    import CONFIG as ignoreAPI
    import Logger
except ImportError:
    raise


log = Logger.getLogger()

# Crypto LiquidationBot


def setAsBitfinex():
    targetExchange = 'bitfinex'
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchange = ccxt.bitfinex(exchangeConfig)
    return exchange


def setAsBittrex():
    targetExchange = 'bittrex'
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchange = ccxt.bittrex(exchangeConfig)
    return exchange


def setAsCryptopia():
    targetExchange = 'cryptopia'
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchange = ccxt.cryptopia(exchangeConfig)
    return exchange


def setAsPoloniex():
    targetExchange = 'poloniex'
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchange = ccxt.poloniex(exchangeConfig)
    return exchange

def setAscoinbase_pro():
    targetExchange = 'coin_base_pro'
    exchangeConfig = ignoreAPI.exchangesKeys[targetExchange]
    exchange = ccxt.coinbasepro(exchangeConfig)
    return exchange

def LIQUIDATETOBASE(base, exchange):
    currencies = exchange.fetchBalance()['free']
    failed = list()
    success = list()
    for currency in currencies:
        amount = currencies[currency]
        market = (currency + base)
        if float(amount) > 0:
            print(market, amount)
            try:
                log.info("Trying to Make Order {}".format((market,
                                                           currency,
                                                           amount)))
                price = exchange.fetch_ticker(market)['ask'] * 1.01
                exchange.create_limit_sell_order(market, amount, price)
                log.info("Order made on {}".format((market,
                                                    currency,
                                                    amount)))
                success.append((market, currency, amount))
            except Exception as e:
                print(e)
            failed.append(currency)
            time.sleep(1)
#        print("Process3ed :{}".format(currency))
    return failed, success


def cancelAllOldOrders(exchange):
    listOfOrders = exchange.fetchOpenOrders()
    ageOfOrder = datetime.timedelta(minutes=60)
    b = datetime.datetime.now() - ageOfOrder
    for order in listOfOrders:
        a = datetime.datetime.fromtimestamp(order['timestamp'] / 1000)
        if b > a:
            exchange.cancel_order(order['id'])
            print("old order found {}".format(order['id']))
        time.sleep(1)


def masterLiquidate():

    #     requiredBases = ['/LTC']
    #     exchange = setAsCryptopia()
    #     for base in requiredBases:
    #         cancelAllOldOrders(exchange)
    #         failures, success = LIQUIDATETOBASE(base, exchange)
    #         log.l.info((failures, success))

    #     requiredBases = ['/BTC']
    #     exchange = setAsBittrex()
    #     for base in requiredBases:
    #         cancelAllOldOrders(exchange)
    #         failures, success = LIQUIDATETOBASE(base, exchange)
    #         log.l.info((failures, success))

    requiredBases = ['/BTC']
    exchange = setAscoinbase_pro()
    for base in requiredBases:
        cancelAllOldOrders(exchange)
        failures, success = LIQUIDATETOBASE(base, exchange)
        log.info((failures, success))

#     requiredBases = ['/LTC']
#     exchange = setAsPoloniex()
#     for base in requiredBases:
#         cancelAllOldOrders(exchange)
#         failures, success = LIQUIDATETOBASE(base, exchange)
#         log.l.info((failures, success))


def main():
    while True:
        masterLiquidate()
        log.error("Loop COmpleted Sleeping")
        time.sleep(260)


if __name__ == "__main__":
    main()
