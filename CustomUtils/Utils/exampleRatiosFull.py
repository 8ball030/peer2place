
# we can now insert our ratios
RATIOS = {'BTC': 11.0,
          'ETH': 11.0,
          'XRP': 5.0,
          'LTC': 5.0,
          'EOS': 5.0,
          'XLM': 5.0,
          'USD': 11.0,
          'BSV': 4.0,
          'TRX': 5.0,
          'IOTA': 5.0,
          'XMR': 5.0,
          'DASH': 5.0,
          'ETC': 5.0,
          'NEO': 5.0,
          'XTZ': 5.0,
          'YOYOW': 0.5,
          'QTUM': 0.5,
          'SAN': 0.5,
          'IQ': 0.5,
          'EDO': 0.5,
          'GNT': 0.5,
          'SNT': 0.5,
          'BCH': 4.0,
          'DATA': 0.5,
          }

RATIOS = {'BTC': 11.0,
          'ETH': 10.5,
          'XRP': 5.0,
          'LTC': 5.0,
          'EOS': 5.0,
          'XLM': 5.0,
          'TUSD': 5.0,
          'PAX': 4.0,
          'BNB': 5.0,
          'BSV': 4.0,
          'TRX': 5.0,
          'IOTA': 5.0,
          'XMR': 5.0,
          'DASH': 5.0,
          'ETC': 5.0,
          'NEO': 5.0,
          'XTZ': 5.0,
          'YOYOW': 0.0,
          'QTUM': 0.5,
          'SAN': 0.5,
          'IQ': 0.0,
          'EDO': 0.0,
          'GNT': 0.0,
          'SNT': 0.0,
          'BCH': 4.0,
          'DATA': 0.5,
          }


# for asset in balances, evaluate for allocation

# then inject ratios


def main():
    print(sum(RATIOS.values()))


if __name__ == '__main__':
    main()
