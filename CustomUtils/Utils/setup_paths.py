"""
This is the "example" module.

The example module supplies one function, factorial().  For example,

>>>setup_paths("All Ok!")
All Ok!
"""
import sys
import os
import doctest

global sys

def setup_paths():
    """Set the paths required by the program. Variables are collected from the global evenironment
    >>> setup_paths() 
    All Ok!
    """
    try:
        sys.path += ["/home/tom/Desktop/mscprojectv2",
                     "/home/tom/Desktop/mscprojectv2/Workers/SimulationService",
                     "/app/CustomUtils",
                     "/app/simulation_service",
                     "/app"
                    ]
    except:
        print("Error importing paths..")
    #import CustomUtils
    print("All Ok!")

setup_paths()
if __name__ == "__main__":
    main()

# class testAccounSetup(TestCase):
#     def 

# if __name__ == '__main__':
#     # trader = setupUpLiveEnviron()
#     SetUpSim = False
#     if SetUpSim:
#         trader = createSimulation(
#             portfolioName="Sim3",
#             strategyName="acrobat",
#             agentName="Sim3",
#             exchangeName="binanceSIM",
#         )
#     else:
#         trader = setupUpLiveEnviron()
