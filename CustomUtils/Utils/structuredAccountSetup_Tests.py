"""
This is the "example" module.

The example module supplies one function, factorial().  For example,

>>>
All Ok!
"""
import doctest
try:
    import setup_paths
except:
    from CustomUtils.Utils import setup_paths
import ccxt
import hashlib
import json

import CustomUtils.CONFIG as CONFIG
from CustomUtils import ormDBInteractor
from CustomUtils.dbModels.out import (Asset, Portfolio, Strategy, Trader, Balance,
                              Base, Exchange, Market, Ratio)


from sqlalchemy.orm import subqueryload
# c1 = session.query(Customer).options(subqueryload(Customer.invoices)).filter_by(name = 'Govind Pant').one()

import os
dbSesh = ormDBInteractor.setUpSession(configSet=os.environ.get("DATABASE"))


from cryptoExchanges import cryptoExchangeFactory

def getExchangeCCXT(*, exchangeName):
    """
    Takes an exchange name and returns an exchange model.
    >>> getExchangeCCXT(exchangeName="binance")
    ccxt.binance()
    """
    availableExchanges = {"binance": ccxt.binance,
                          "coin_base_pro": ccxt.coinbasepro,
                          "binanceSIM": ccxt.binance,
                          "bitfinex": ccxt.bitfinex,
                          "exchangeName": ccxt.binance,
                          "deribit": ccxt.deribit,
                          }
    exchangeConfig = CONFIG.exchangesKeys[exchangeName]
    return availableExchanges[exchangeName](exchangeConfig)

def hashArgs(args, required=None):
    """
    Generats a uuid from a givn arguments, it must sort th args by key to prevent duplicates
    >>> args = ["binance"]
    >>> print(hashArgs(*args))
    2087b32b0078525b7481800d45ba650a

    We need to take lists as well

    >>> argss = ["binance", "china"]
    >>> print(hashArgs(argss))
    d3022f97a93050f9bf62da6b1d9ddb67
    >>> argss = ["china", "binance",]

    We a list in revers will pass with the same UUID
    >>> print(hashArgs(argss))
    d3022f97a93050f9bf62da6b1d9ddb67

    """
    if required == None:
        uuArgs = sorted(map(lambda x: str(x), args))
    return str(hashlib.md5("".join(uuArgs).encode("utf8")).hexdigest())


def buildExchange(*, exchangeName, location, feeAmount):
    """Create a new Exchange.
    We first grab the config of the exchange
    >>> exchangeModel = buildExchange(exchangeName="binance", location="malta", feeAmount=0.1)
    """
    id = hashArgs([exchangeName, location, feeAmount])
    ex = Exchange(id=id,
                  name=exchangeName,
                  location=location,
                  feeAmount=feeAmount)
    try:
        dbSesh.query(Exchange).filter(Exchange.id == id).one()
    except Exception as E:
        print("Exchange Doesnt Exist Exisited!", E)
        dbSesh.add(ex)
        dbSesh.commit()
    return ex

def buildAssets(*, exchangeModel, exchangeApi):
    """Create a new Exchange.
    We first grab the config of the exchange
    >>> params = dict(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> exchangeModel = buildExchange(**params)
    >>> print(exchangeModel.id)
    3840e010a0111146d09f8916d39711fa
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> assets = buildAssets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> btcModel = [(print(a.name), a)[1] for a in assets if "BTC" == a.name][0]
    BTC
    """
    uniqu = list()
    for market in exchangeApi.fetch_markets():
        if market['base'] not in uniqu:
            uniqu.append(market['base'])
        if market['quote'] not in uniqu:
            uniqu.append(market['quote'])
    assets = []
    for a in uniqu:
        id = hashArgs([a, exchangeModel.id])
        asset = Asset(id=id, name=a,
                      symbol=a, exchangeId=exchangeModel.id)
        try:
            dbSesh.query(Asset).filter(Asset.id == id and
                                        Asset.exchangeId == exModel.id).one()
        except Exception as E:
                print("Asset Doesnt Exist Exisited!", E)
                dbSesh.add(asset)
                dbSesh.commit()
        finally:
            assets.append(asset)
    return assets


def buildMarkets(*, exchangeModel, exchangeApi):
    """Create all Markets.
    We first grab the config of the exchange
    >>> exchangeModel = buildExchange(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> markets = buildMarkets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> btcModel = [(print(a.name), a)[1] for a in markets if a.name.find("FET") >= 0]
    FET/BNB
    FET/BTC
    FET/USDT
    """
    markets = exchangeApi.fetchMarkets()
    ret = []
    for market in markets:
        (market["base"], exchangeModel.id)
        base = dbSesh.query(Asset).filter(
            Asset.name == market["base"]).filter(
            Asset.exchangeId == exchangeModel.id).one()
        quote = dbSesh.query(Asset).filter(
            Asset.name == market["quote"]).filter(
            Asset.exchangeId == exchangeModel.id).one()
        id = hashArgs([base.name, quote.name, exchangeModel.id, market['id']])
        m = Market(id=id,
                   exchangeID=exchangeModel.id,
                   name=market['symbol'],
                   baseAssetID=base.id, counterAssetID=quote.id,
                   minimumTrade=market['limits']['amount']['min'])
        try:
            dbSesh.query(Market).filter(Market.id == id).one()
        except Exception as E:
            print("Asset Doesnt Exist Exisited!", E)
            dbSesh.add(m)
            dbSesh.commit()
        finally:
            ret.append(m)
    return ret


def buildPortfolio(*, exchangeModel, portfolioName):
    """Create all Markets.
    We first grab the config of the exchange
    >>> params = dict(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> exchangeModel = buildExchange(**params)
    >>> p = buildPortfolio(exchangeModel=exchangeModel, portfolioName="TEST")
    >>> print(p.id)
    94b85aaa947c556163579f763425c36e
    """
    id = hashArgs([exchangeModel.id, portfolioName])
    portfolio = Portfolio(id=id,
                          exchangeID=exchangeModel.id,
                          startingValue=500,
                          description=portfolioName)
    try:
        portfolio = dbSesh.query(
            Portfolio).filter(Portfolio.id == id and
                              Portfolio.exchangeID == excModel.id).one()
    except Exception as E:
        ("Portfolio Doesnt Exist Exisited!", portfolioName)
        dbSesh.rollback()
        dbSesh.add(portfolio)
        dbSesh.commit()
    finally:
        dbSesh.rollback()
    return portfolio


def buildStrategy(*, params, strategyName):
    """
    Building the strategy models.
    >>> params = dict(ratioThreshold=0.1, buyOffset=0.02, sellOffset=0.02, buyPreference=0.1, orderSizeType="minimum", orderType="limit")
    >>> s = buildStrategy(params=params, strategyName="TEST")
    >>> print(s.id)
    520a7cfa479be012972c88dd3dd0cafb
    """
    id = hashArgs(params)
    S = Strategy(id=id,
                 description=strategyName,
                 paramsJSON=json.dumps(params))
    try:
        S = dbSesh.query(Strategy).filter(Strategy.id == id).one()
    except Exception as E:
        ("Strategy Doesnt Exist Exisited!", strategyName)
        dbSesh.add(S)
        dbSesh.commit()
    finally:
        dbSesh.rollback()
    return S


def updateBalances(*, exchangeModel, exchangeApi):
    """
    Update balances live from the exchange.
    >>> exchangeModel = buildExchange(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> results = updateBalances(exchangeModel=exchangeModel, exchangeApi=api)
    >>> len(results) >= 0
    True
    """
    ret = []
    for cur, bal in exchangeApi.fetchBalance()['total'].items():
        # first we map to an asset
        if cur in ["ADD", "ATD", "MTO", "EON", "MEETONE", "EOP", "IQ", "LDBTC"]:
            continue
        try:
            a = dbSesh.query(Asset).filter(Asset.name == cur).filter(Asset.exchangeId == exchangeModel.id).one()
            a.total = bal
            ret.append(a)
            dbSesh.merge(a)
            dbSesh.commit()
        except Exception as E:
            print("Assets Not FOund!!!!!!!", cur)
        finally:
            dbSesh.rollback()
    return ret



def buildRatios(*, allowedBal, portfolioModel, exchangeModel):
    """
    Build ratios from exchance model, allowedbalances and portfolio model

    >>> exchangeModel = buildExchange(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> params = dict(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> allowedBal = {"BTC": 0.5, "USDT":0.5}
    >>> exchangeModel = buildExchange(**params)
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> portfolioModel = buildPortfolio(exchangeModel=exchangeModel, portfolioName="TEST")
    >>> ratios = buildRatios(allowedBal=allowedBal, portfolioModel=portfolioModel, exchangeModel=exchangeModel)
    >>> print(len(ratios))
    2
    """
    ret = []
    for k, v in allowedBal.items():
        asset = dbSesh.query(
                 Asset).filter(
                     Asset.name == k).filter(Asset.exchangeId == exchangeModel.id).one()
        id = hashArgs([k, portfolioModel.id])
        try:
            # first we try to find the record
            ratio = dbSesh.query(Ratio).filter(Ratio.id == id).one()
            ratio.allowedRatio = v
            dbSesh.merge(ratio)
            dbSesh.commit()
        except Exception as E:
            ratio = Ratio(id=id,
            assetId=asset.id,
            portfolioId=portfolioModel.id,
            allowedRatio=v)
            dbSesh.add(ratio)
            print("Transaction failed with error :", E, k)
            dbSesh.commit()
        finally:
            dbSesh.rollback()
            ret.append(ratio)
    return ret




def buildTrader(*, portfolioModel, exchangeModel, strategyModel, agentName, params):
    """
    >>> exchangeModel = buildExchange(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> params = dict(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> allowedBal = {"BTC": 0.5, "USDT":0.5}
    >>> exchangeModel = buildExchange(**params)
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> portfolioModel = buildPortfolio(exchangeModel=exchangeModel, portfolioName="TEST")
    >>> params = dict(ratioThreshold=0.1, buyOffset=0.02, sellOffset=0.02, buyPreference=0.1, orderSizeType="minimum", orderType="limit")
    >>> strategyModel = buildStrategy(params=params, strategyName="TEST")
    >>> trader = buildTrader(portfolioModel=portfolioModel, exchangeModel=exchangeModel, strategyModel=strategyModel, agentName="TEST", params=None)
    Trader Id : e024b33af2de43031d1399547cd066c2
    """
    id = hashArgs([portfolioModel.id, strategyModel.id, exchangeModel.id, agentName, params])
    print(f"Trader Id : {id}")
    with dbSesh.no_autoflush as db:
        try:
            trader = db.query(Trader).options(subqueryload('*')).filter(Trader.id == id).one()
        except Exception as E:
            trader = Trader(id=id,
                            portfolioId=portfolioModel.id,
                            strategyId=strategyModel.id,
                            exchangeId=exchangeModel.id,
                            agentName=agentName,
                            params=json.dumps(params))
            db.add(trader)
            db.commit()
        finally:
            db.rollback()
            trader = db.query(Trader).options(subqueryload('*')).filter(Trader.id == id).one()
    return trader

def liveEnvironmentCreationBinance(*, exchangeName, traderId):
    """ We create a fresh Trader agent model from the exchange name alone
    >>> params = dict(exchangeName="binance", location="malta", feeAmount=0.1)
    >>> allowedBal = {"BTC": 0.5, "USDT":0.5}
    >>> exchangeModel = buildExchange(**params)
    >>> api = getExchangeCCXT(exchangeName="binance")
    >>> portfolioModel = buildPortfolio(exchangeModel=exchangeModel, portfolioName="LIVE")
    >>> params = dict(ratioThreshold=0.1, buyOffset=0.02, sellOffset=0.02, buyPreference=0.1, orderSizeType="minimum", orderType="limit")
    >>> strategyModel = buildStrategy(params=params, strategyName="LIVE")
    >>> trader = buildTrader(portfolioModel=portfolioModel, exchangeModel=exchangeModel, strategyModel=strategyModel, agentName="LIVE", params=None)
    Trader Id : 616bbc77f43d0ba859a0fc50c649d301
    >>> print(liveEnvironmentCreationBinance(exchangeName="binance", traderId=trader.id).id)
    616bbc77f43d0ba859a0fc50c649d301
    """
    return dbSesh.query(Trader).options(subqueryload('*')).filter(Trader.id == traderId).one()

def liveEnvironmentCreationCoinBase():
    """ We create a fresh Trader agent model from the exchange name alone
    >>> allowedBal = {"BTC": 0.5, "USDT":0.5}
    >>> exchangeModel = buildExchange(exchangeName="coin_base_pro", location="UK", feeAmount=0.5)
    >>> api = getExchangeCCXT(exchangeName="coin_base_pro")
    >>> portfolioModel = buildPortfolio(exchangeModel=exchangeModel, portfolioName="LIVE")
    >>> params = dict(ratioThreshold=0.1, buyOffset=0.02, sellOffset=0.02, buyPreference=0.1, orderSizeType="minimum", orderType="limit")
    >>> strategyModel = buildStrategy(params=params, strategyName="LIVE")
    >>> trader = buildTrader(portfolioModel=portfolioModel, exchangeModel=exchangeModel, strategyModel=strategyModel, agentName="LIVE", params=None)
    Trader Id : 90d0fec8d8eae9e46de287f2098a28ac
    >>> print(liveEnvironmentCreationBinance(exchangeName="coin_base_pro", traderId=trader.id).id)
    90d0fec8d8eae9e46de287f2098a28ac
    >>> assets = buildAssets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> markets = buildMarkets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> results = updateBalances(exchangeModel=exchangeModel, exchangeApi=api)
    Assets Not FOund!!!!!!! ZIL
    Assets Not FOund!!!!!!! MKR
    >>> len(results) >= 0
    True
    >>> curr = "BAT/ETH"
    >>> [a.name for a in markets if a.name == curr]
    ['BAT/ETH']
    >>> results = updateBalances(exchangeModel=exchangeModel, exchangeApi=api)
    Assets Not FOund!!!!!!! ZIL
    Assets Not FOund!!!!!!! MKR
    """

def liveEnvironmentCreationDeribit():
    """ We create a fresh Trader agent model from the exchange name alone
    >>> allowedBal = {"BTC": 0.5, "USDT":0.5}
    >>> exchangeModel = buildExchange(exchangeName="deribit", location="belgium", feeAmount=-0.2)
    >>> api = getExchangeCCXT(exchangeName="deribit")
    >>> portfolioModel = buildPortfolio(exchangeModel=exchangeModel, portfolioName="LIVE")
    >>> params = dict(ratioThreshold=0.1, buyOffset=0.02, sellOffset=0.02, buyPreference=0.1, orderSizeType="minimum", orderType="limit")
    >>> strategyModel = buildStrategy(params=params, strategyName="LIVE")
    >>> trader = buildTrader(portfolioModel=portfolioModel, exchangeModel=exchangeModel, strategyModel=strategyModel, agentName="LIVE", params=None)
    Trader Id : d28e869da995516cd5361332a5986d2d
    >>> print(liveEnvironmentCreationBinance(exchangeName="coin_base_pro", traderId=trader.id).id)
    d28e869da995516cd5361332a5986d2d
    >>> assets = buildAssets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> markets = buildMarkets(exchangeModel=exchangeModel, exchangeApi=api)
    >>> results = updateBalances(exchangeModel=exchangeModel, exchangeApi=api)
    >>> len(results) >= 0
    True
    >>> curr = "BAT/ETH"
    >>> [a.name for a in markets if a.name == curr]
    []
    >>> results = updateBalances(exchangeModel=exchangeModel, exchangeApi=api)
    """
def error_1_check_has_exchange():
    """

    """

def main():
    doctest.testmod()

if __name__ == "__main__":
    main()

# class testAccounSetup(TestCase):
#     def

# if __name__ == '__main__':
#     # trader = setupUpLiveEnviron()
#     SetUpSim = False
#     if SetUpSim:
#         trader = createSimulation(
#             portfolioName="Sim3",
#             strategyName="acrobat",
#             agentName="Sim3",
#             exchangeName="binanceSIM",
#         )
#     else:
#         trader = setupUpLiveEnviron()
