
CREATE TABLE "public"."Traders" (
    "id" VARCHAR(50)   NOT NULL,
    "portfolioId" VARCHAR(50)   NOT NULL,
    "strategyId" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "agentName" VARCHAR(50)   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "public"."Assets" (
    "id" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "name" VARCHAR(50)   NOT NULL,
    "symbol" VARCHAR(50)   NOT NULL
);

CREATE TABLE "public"."Orders" (
    "id" VARCHAR(50)   NOT NULL,
    "marketId" VARCHAR(50)   NOT NULL,
    "traderId" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "parentOrderId" VARCHAR(50)   NOT NULL,
    "amountBase" FLOAT   NOT NULL,
    "amountCounter" FLOAT   NOT NULL,
    "amountUSD" FLOAT   NOT NULL,
    "status" VARCHAR(50)   NOT NULL,
    "type" VARCHAR(15)   NOT NULL,
    "side" VARCHAR(4)   NOT NULL,
    "rate" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "public"."Exchanges" (
    "id" VARCHAR(50)   NOT NULL,
    "name" INT   NOT NULL,
    "location" VARCHAR(50)   NOT NULL,
    "feeAmount" FLOAT   NOT NULL
);

CREATE TABLE "public"."Markets" (
    "id" VARCHAR(50)   NOT NULL,
    "baseAssetID" VARCHAR(50)   NOT NULL,
    "counterAssetID" VARCHAR(50)   NOT NULL,
    "exchangeID" VARCHAR(50)   NOT NULL,
    "minimumTrade" FLOAT   NOT null
    "name" VARCHAR(50)
);

CREATE TABLE "public"."Portfolios" (
    "id" VARCHAR(50)   NOT NULL,
    "exchangeID" VARCHAR(50)   NOT NULL,
    "startingValue" FLOAT   NOT NULL,
    "description" VARCHAR(500)   NOT NULL
);

CREATE TABLE "public"."Balances" (
    "id" VARCHAR(100)   NOT NULL,
    "assetID" VARCHAR(100)   NOT NULL,
    "portfolioID" VARCHAR(100)   NOT NULL,
    "total" FLOAT   NOT NULL,
    "allowedRatio" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "public"."Strategies" (
    "id" VARCHAR(100)   NOT NULL,
    "description" VARCHAR(100)   NOT NULL,
    "paramsJSON" VARCHAR(1000)   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "public"."Snapshots" (
    "id" VARCHAR(100)   NOT NULL,
    "portfolioId" VARCHAR(100)   NOT NULL,
    "currentValue" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL
);


CREATE TABLE "public"."Ratios" (
    "id" VARCHAR(100)   NOT NULL,
    "portfolioId" VARCHAR(100)   NOT NULL,
    "assetId" VARCHAR(100)   NOT NULL,
    "allowedRatio" FLOAT   NOT NULL,
    "currentRatio" FLOAT not null,
    "createdOn" TIMESTAMP  DEFAULT now() NOT null,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT null
);


ALTER TABLE "public"."Portfolios" ADD CONSTRAINT portfolios_pk PRIMARY KEY (id);

ALTER TABLE "public"."Exchanges" ADD CONSTRAINT exchanges_pk PRIMARY KEY (id);

ALTER TABLE "public"."Orders" ADD CONSTRAINT orders_pk PRIMARY KEY (id);

ALTER TABLE "public"."Traders" ADD CONSTRAINT traders_pk PRIMARY KEY (id);

ALTER TABLE "public"."Markets" ADD CONSTRAINT markets_pk PRIMARY KEY (id);

ALTER TABLE "public"."Assets" ADD CONSTRAINT assets_pk PRIMARY KEY (id);

ALTER TABLE "public"."Balances" ADD CONSTRAINT balances_pk PRIMARY KEY (id);

ALTER TABLE "public"."Snapshots" ADD CONSTRAINT shapshots_pk PRIMARY KEY (id);

ALTER TABLE "public"."Strategies" ADD CONSTRAINT strategy_pk PRIMARY KEY (id);

ALTER TABLE "public"."Ratios" ADD CONSTRAINT ratios_pk PRIMARY KEY (id);

ALTER TABLE "public"."Ratios" ADD CONSTRAINT "fk_Ratios_portfolioID" FOREIGN KEY("portfolioId")
REFERENCES "public"."Portfolios" ("id");

ALTER TABLE "public"."Ratios" ADD CONSTRAINT "fk_Ratios_assets" FOREIGN KEY("assetId")
REFERENCES "public"."Assets" ("id");

ALTER TABLE "public"."Traders" ADD CONSTRAINT "fk_Traders_portfolioId" FOREIGN KEY("portfolioId")
REFERENCES "public"."Portfolios" ("id");


ALTER TABLE "public"."Traders" ADD CONSTRAINT "fk_Traders_strategyId" FOREIGN KEY("strategyId")
REFERENCES "public"."Strategies" ("id");



ALTER TABLE "public"."Traders" ADD CONSTRAINT "fk_Traders_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "public"."Exchanges" ("id");

ALTER TABLE "public"."Assets" ADD CONSTRAINT "fk_Assets_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "public"."Exchanges" ("id");

ALTER TABLE "public"."Orders" ADD CONSTRAINT "fk_Orders_marketId" FOREIGN KEY("marketId")
REFERENCES "public"."Markets" ("id");

ALTER TABLE "public"."Orders" ADD CONSTRAINT "fk_Orders_traderId" FOREIGN KEY("traderId")
REFERENCES "public"."Traders" ("id");

ALTER TABLE "public"."Orders" ADD CONSTRAINT "fk_Orders_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "public"."Exchanges" ("id");


ALTER TABLE "public"."Orders" ADD CONSTRAINT "fk_Orders_parentOrderId" FOREIGN KEY("parentOrderId")
REFERENCES "public"."Orders" ("id");

ALTER TABLE "public"."Markets" ADD CONSTRAINT "fk_Markets_baseAssetID" FOREIGN KEY("baseAssetID")
REFERENCES "public"."Assets" ("id");

ALTER TABLE "public"."Markets" ADD CONSTRAINT "fk_Markets_counterAssetID" FOREIGN KEY("counterAssetID")
REFERENCES "public"."Assets" ("id");

ALTER TABLE "public"."Markets" ADD CONSTRAINT "fk_Markets_exchangeID" FOREIGN KEY("exchangeID")
REFERENCES "public"."Exchanges" ("id");

ALTER TABLE "public"."Portfolios" ADD CONSTRAINT "fk_Portfolios_exchangeID" FOREIGN KEY("exchangeID")
REFERENCES "public"."Exchanges" ("id");

ALTER TABLE "public"."Balances" ADD CONSTRAINT "fk_Balances_assetID" FOREIGN KEY("assetID")
REFERENCES "public"."Assets" ("id");

ALTER TABLE "public"."Balances" ADD CONSTRAINT "fk_Balances_portfolioID" FOREIGN KEY("portfolioID")
REFERENCES "public"."Portfolios" ("id");



ALTER TABLE "public"."Snapshots" ADD CONSTRAINT "fk_Snapshots_portfolioId" FOREIGN KEY("portfolioId")
REFERENCES "public"."Portfolios" ("id");

--CREATE INDEX "idx_Assets_name"
--ON "Assets" ("name");











select "name" "Market", "side" "Side",
	"amountBase" "Units in Base","status" "Status", "type" "Type", "createdOn" "Date Created"  from "public"."Orders" 
join "public"."Markets" on "marketId" = "Markets"."id"

inner join "public"."Markets" on "Orders"."marketId" = "Markets"."id"



select
       "name" "Market",
	   "side" "Side",
       "amountBase" "Units in Base",
       "status" "Status", 
       "type" "Type", 
       "createdOn" "Date Created"
from 
	"public"."Orders"
inner join 
	"public"."Markets" on "public"."Orders"."marketId" = "Markets"."id"

	


SELECT
  "createdOn" "time",
  "currentValue"
FROM "public"."Snapshots"
ORDER BY "createdOn"

select  from "public"."Ratios"
inner join "public"."Assets" on "public"."Asset"."id" = "Ratios"."assetId" 





CREATE TABLE `rawURLS` (
  `Address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `root_domain` text COLLATE utf8_unicode_ci,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


