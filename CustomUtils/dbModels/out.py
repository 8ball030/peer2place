# coding: utf-8
from sqlalchemy import Column, DateTime, Float, ForeignKey, String, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Exchange(Base):
    __tablename__ = 'Exchanges'

    id = Column(String(50), primary_key=True)
    name = Column(String(50), nullable=False)
    location = Column(String(50), nullable=False)
    feeAmount = Column(Float(53), nullable=False)


class Strategy(Base):
    __tablename__ = 'Strategies'

    id = Column(String(100), primary_key=True)
    description = Column(String(100), nullable=False)
    paramsJSON = Column(String(1000), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))
    updatedOn = Column(DateTime, nullable=False, server_default=text("now()"))


class Asset(Base):
    __tablename__ = 'Assets'

    id = Column(String(50), primary_key=True)
    exchangeId = Column(ForeignKey('Exchanges.id'), nullable=False)
    name = Column(String(50), nullable=False)
    symbol = Column(String(50), nullable=False)

    Exchange = relationship('Exchange', uselist=True)


class Portfolio(Base):
    __tablename__ = 'Portfolios'

    id = Column(String(50), primary_key=True)
    exchangeID = Column(ForeignKey('Exchanges.id'), nullable=False)
    startingValue = Column(Float(53), nullable=False)
    description = Column(String(500), nullable=False)

    Exchange = relationship('Exchange')


class TradeEvent(Base):
    __tablename__ = 'TradeEvents'

    id = Column(String(50), primary_key=True)
    exchangeId = Column(ForeignKey('Exchanges.id'), nullable=False)
    market = Column(String(50), nullable=False)
    amount = Column(Float(53), nullable=False)
    rate = Column(Float(53), nullable=False)
    timeStamp = Column(DateTime(True), nullable=False, server_default=text("now()"))

    Exchange = relationship('Exchange')


class Balance(Base):
    __tablename__ = 'Balances'

    id = Column(String(100), primary_key=True)
    assetID = Column(ForeignKey('Assets.id'), nullable=False)
    portfolioID = Column(ForeignKey('Portfolios.id'), nullable=False)
    total = Column(Float(53), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))
    updatedOn = Column(DateTime, nullable=False, server_default=text("now()"))
    available = Column(Float(53), nullable=False)

    Asset = relationship('Asset')
    Portfolio = relationship('Portfolio')


class Market(Base):
    __tablename__ = 'Markets'

    id = Column(String(50), primary_key=True)
    baseAssetID = Column(ForeignKey('Assets.id'), nullable=False)
    counterAssetID = Column(ForeignKey('Assets.id'), nullable=False)
    exchangeID = Column(ForeignKey('Exchanges.id'), nullable=False)
    minimumTrade = Column(Float(53), nullable=False)
    name = Column(String, nullable=False)

    Asset = relationship('Asset', primaryjoin='Market.baseAssetID == Asset.id')
    Asset1 = relationship('Asset', primaryjoin='Market.counterAssetID == Asset.id')
    Exchange = relationship('Exchange')


class Ratio(Base):
    __tablename__ = 'Ratios'

    id = Column(String(100), primary_key=True)
    portfolioId = Column(ForeignKey('Portfolios.id'), nullable=False)
    assetId = Column(ForeignKey('Assets.id'), nullable=False)
    allowedRatio = Column(Float(53), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))
    updatedOn = Column(DateTime, nullable=False, server_default=text("now()"))
    currentRatio = Column(Float(53))

    Asset = relationship('Asset')
    Portfolio = relationship('Portfolio')


class Snapshot(Base):
    __tablename__ = 'Snapshots'

    id = Column(String(100), primary_key=True)
    portfolioId = Column(ForeignKey('Portfolios.id'), nullable=False)
    currentValue = Column(Float(53), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))

    Portfolio = relationship('Portfolio')


class Trader(Base):
    __tablename__ = 'Traders'

    id = Column(String(50), primary_key=True)
    portfolioId = Column(ForeignKey('Portfolios.id'), nullable=False)
    strategyId = Column(ForeignKey('Strategies.id'), nullable=False)
    exchangeId = Column(ForeignKey('Exchanges.id'), nullable=False)
    agentName = Column(String(50), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))
    updatedOn = Column(DateTime, nullable=False, server_default=text("now()"))
    params = Column(String(500))

    Exchange = relationship('Exchange')
    Portfolio = relationship('Portfolio')
    Strategy = relationship('Strategy')


class Order(Base):
    __tablename__ = 'Orders'

    id = Column(String(50), primary_key=True)
    marketId = Column(ForeignKey('Markets.id'), nullable=False)
    traderId = Column(ForeignKey('Traders.id'), nullable=False)
    exchangeId = Column(ForeignKey('Exchanges.id'), nullable=False)
    parentOrderId = Column(ForeignKey('Orders.id'))
    amountBase = Column(Float(53), nullable=False)
    amountCounter = Column(Float(53), nullable=False)
    amountUSD = Column(Float(53), nullable=False)
    status = Column(String(50), nullable=False)
    type = Column(String(15), nullable=False)
    side = Column(String(4), nullable=False)
    rate = Column(Float(53), nullable=False)
    createdOn = Column(DateTime, nullable=False, server_default=text("now()"))
    updatedOn = Column(DateTime, nullable=False, server_default=text("now()"))
    idfromexchange = Column(String)

    Exchange = relationship('Exchange')
    Market = relationship('Market')
    parent = relationship('Order', remote_side=[id])
    Trader = relationship('Trader')
