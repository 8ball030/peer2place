
import CONFIG


class DBInteractor:
    def __init__(self,
                 dbType=None,
                 dbName=None,
                 collectionName=None):

        if dbType == 'DockerMYSQL':
            params = CONFIG.DATABASES['DockerMYSQL']
            self.__SQLConnect(params)

        elif dbType == 'DockerMongo':
            params = CONFIG.DATABASES['DockerMongo']
            self.__MongoConnect(params)
            self.db = self.client.get_database(dbName)
            self.coll = self.db.get_collection(collectionName)
        else:
            raise

    def __SQLConnect(self, params):
        import pymysql
        con = pymysql.connect(host=params['host'],
                              user=params['sql_username'],
                              passwd=params['sql_password'],
                              port=params['port'],
                              db=params['sql_main_database'],
                              autocommit=True,
                              cursorclass=pymysql.cursors.DictCursor,
                              )
        self.db = con
        self.cursor = con.cursor()

    def __killTunnel(self):
        self.tunnel.close()

    def __MongoConnect(self, params):
        import pymongo
        self.client = pymongo.MongoClient(host=params['host'],
                                          port=params['port'])
        print("Client connected")

    def runQuery(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    # def __createTunnel(self, params):
    #     import paramiko
    #     from sshtunnel import SSHTunnelForwarder
    #     mypkey = paramiko.RSAKey.from_private_key_file(params['ssh_key_path'])
    #     self.tunnel = SSHTunnelForwarder(
    #         (params['ssh_host'], params['ssh_port']),
    #         ssh_username=params['ssh_user'],
    #         ssh_pkey=mypkey,
    #         remote_bind_address=(params['sql_hostname'],
    #                              params['sql_port']))
    #     self.tunnel.start()
