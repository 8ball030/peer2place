#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 09:13:18 2018

custom logger, will create a main.log file

@author: tom
"""

import logging


def getLogger(console=True, file=True):
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s',
        datefmt='%d-%m-%Y %H:%M:%S'
    )
    log = logging.getLogger('/app/CustomUtils/mainLog.log')
    log.setLevel(logging.DEBUG)
    if file is True:
        fileHandler = logging.FileHandler('/app/CustomUtils/mainLog.log', 'a')
        fileHandler.setLevel(logging.DEBUG)
        fileHandler.setFormatter(formatter)
        log.addHandler(fileHandler)
    if console is False:
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        consoleHandler.setFormatter(formatter)
        log.addHandler(consoleHandler)
    return log


if __name__ == "__main__":
    log = getLogger()
    log.info("Test")
