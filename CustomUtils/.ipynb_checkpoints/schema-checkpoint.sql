
CREATE TABLE "traderState"."Traders" (
    "id" VARCHAR(50)   NOT NULL,
    "portfolioId" VARCHAR(50)   NOT NULL,
    "strategyId" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "agentName" VARCHAR(50)   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "traderState"."Assets" (
    "id" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "name" VARCHAR(50)   NOT NULL,
    "symbol" VARCHAR(50)   NOT NULL
);

CREATE TABLE "traderState"."Orders" (
    "id" VARCHAR(50)   NOT NULL,
    "marketId" VARCHAR(50)   NOT NULL,
    "traderId" VARCHAR(50)   NOT NULL,
    "exchangeId" VARCHAR(50)   NOT NULL,
    "parentOrderId" VARCHAR(50)   NOT NULL,
    "amountBase" FLOAT   NOT NULL,
    "amountCounter" FLOAT   NOT NULL,
    "amountUSD" FLOAT   NOT NULL,
    "status" VARCHAR(50)   NOT NULL,
    "type" VARCHAR(15)   NOT NULL,
    "side" VARCHAR(4)   NOT NULL,
    "rate" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "traderState"."Exchanges" (
    "id" VARCHAR(50)   NOT NULL,
    "name" INT   NOT NULL,
    "location" VARCHAR(50)   NOT NULL,
    "feeAmount" FLOAT   NOT NULL
);

CREATE TABLE "traderState"."Markets" (
    "id" VARCHAR(50)   NOT NULL,
    "baseAssetID" VARCHAR(50)   NOT NULL,
    "counterAssetID" VARCHAR(50)   NOT NULL,
    "exchangeID" VARCHAR(50)   NOT NULL,
    "minimumTrade" FLOAT   NOT NULL
);

CREATE TABLE "traderState"."Portfolios" (
    "id" VARCHAR(50)   NOT NULL,
    "exchangeID" VARCHAR(50)   NOT NULL,
    "startingValue" FLOAT   NOT NULL,
    "description" VARCHAR(500)   NOT NULL
);

CREATE TABLE "traderState"."Balances" (
    "id" VARCHAR(100)   NOT NULL,
    "assetID" VARCHAR(100)   NOT NULL,
    "portfolioID" VARCHAR(100)   NOT NULL,
    "total" FLOAT   NOT NULL,
    "allowedRatio" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "traderState"."Strategies" (
    "id" VARCHAR(100)   NOT NULL,
    "description" VARCHAR(100)   NOT NULL,
    "paramsJSON" VARCHAR(1000)   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT NULL
);

CREATE TABLE "traderState"."Snapshots" (
    "id" VARCHAR(100)   NOT NULL,
    "portfolioId" VARCHAR(100)   NOT NULL,
    "currentValue" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT NULL
);


CREATE TABLE "traderState"."Ratios" (
    "id" VARCHAR(100)   NOT NULL,
    "portfolioId" VARCHAR(100)   NOT NULL,
    "assetId" VARCHAR(100)   NOT NULL,
    "allowedRatio" FLOAT   NOT NULL,
    "createdOn" TIMESTAMP  DEFAULT now() NOT null,
    "updatedOn" TIMESTAMP  DEFAULT now() NOT null
);


ALTER TABLE "traderState"."Portfolios" ADD CONSTRAINT portfolios_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Exchanges" ADD CONSTRAINT exchanges_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Orders" ADD CONSTRAINT orders_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Traders" ADD CONSTRAINT traders_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Markets" ADD CONSTRAINT markets_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Assets" ADD CONSTRAINT assets_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Balances" ADD CONSTRAINT balances_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Snapshots" ADD CONSTRAINT shapshots_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Strategies" ADD CONSTRAINT strategy_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Ratios" ADD CONSTRAINT ratios_pk PRIMARY KEY (id);

ALTER TABLE "traderState"."Ratios" ADD CONSTRAINT "fk_Ratios_portfolioID" FOREIGN KEY("portfolioId")
REFERENCES "traderState"."Portfolios" ("id");

ALTER TABLE "traderState"."Ratios" ADD CONSTRAINT "fk_Ratios_assets" FOREIGN KEY("assetId")
REFERENCES "traderState"."Assets" ("id");

ALTER TABLE "traderState"."Traders" ADD CONSTRAINT "fk_Traders_portfolioId" FOREIGN KEY("portfolioId")
REFERENCES "traderState"."Portfolios" ("id");


ALTER TABLE "traderState"."Traders" ADD CONSTRAINT "fk_Traders_strategyId" FOREIGN KEY("strategyId")
REFERENCES "traderState"."Strategies" ("id");



ALTER TABLE "traderState"."Traders" ADD CONSTRAINT "fk_Traders_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "traderState"."Exchanges" ("id");

ALTER TABLE "traderState"."Assets" ADD CONSTRAINT "fk_Assets_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "traderState"."Exchanges" ("id");

ALTER TABLE "traderState"."Orders" ADD CONSTRAINT "fk_Orders_marketId" FOREIGN KEY("marketId")
REFERENCES "traderState"."Markets" ("id");

ALTER TABLE "traderState"."Orders" ADD CONSTRAINT "fk_Orders_traderId" FOREIGN KEY("traderId")
REFERENCES "traderState"."Traders" ("id");

ALTER TABLE "traderState"."Orders" ADD CONSTRAINT "fk_Orders_exchangeId" FOREIGN KEY("exchangeId")
REFERENCES "traderState"."Exchanges" ("id");


ALTER TABLE "traderState"."Orders" ADD CONSTRAINT "fk_Orders_parentOrderId" FOREIGN KEY("parentOrderId")
REFERENCES "traderState"."Orders" ("id");

ALTER TABLE "traderState"."Markets" ADD CONSTRAINT "fk_Markets_baseAssetID" FOREIGN KEY("baseAssetID")
REFERENCES "traderState"."Assets" ("id");

ALTER TABLE "traderState"."Markets" ADD CONSTRAINT "fk_Markets_counterAssetID" FOREIGN KEY("counterAssetID")
REFERENCES "traderState"."Assets" ("id");

ALTER TABLE "traderState"."Markets" ADD CONSTRAINT "fk_Markets_exchangeID" FOREIGN KEY("exchangeID")
REFERENCES "traderState"."Exchanges" ("id");

ALTER TABLE "traderState"."Portfolios" ADD CONSTRAINT "fk_Portfolios_exchangeID" FOREIGN KEY("exchangeID")
REFERENCES "traderState"."Exchanges" ("id");

ALTER TABLE "traderState"."Balances" ADD CONSTRAINT "fk_Balances_assetID" FOREIGN KEY("assetID")
REFERENCES "traderState"."Assets" ("id");

ALTER TABLE "traderState"."Balances" ADD CONSTRAINT "fk_Balances_portfolioID" FOREIGN KEY("portfolioID")
REFERENCES "traderState"."Portfolios" ("id");



ALTER TABLE "traderState"."Snapshots" ADD CONSTRAINT "fk_Snapshots_portfolioId" FOREIGN KEY("portfolioId")
REFERENCES "traderState"."Portfolios" ("id");

--CREATE INDEX "idx_Assets_name"
--ON "Assets" ("name");
