# coding: utf-8
from sqlalchemy import Column, Float, ForeignKey, String, TIMESTAMP, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER, VARCHAR
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Asset(Base):
    __tablename__ = 'Assets'

    assetID = Column(String(100), primary_key=True)
    name = Column(String(100), nullable=False, index=True)
    symbol = Column(String(100), nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class Exchange(Base):
    __tablename__ = 'Exchanges'

    exchangeID = Column(String(100), primary_key=True)
    name = Column(String(50), nullable=False)
    location = Column(String(100), nullable=False)
    feeAmount = Column(Float, nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class Portfolio(Base):
    __tablename__ = 'Portfolios'

    portfolioID = Column(String(100), primary_key=True)
    currentValue = Column(Float, nullable=False)
    startingValue = Column(Float, nullable=False)


class Strategy(Base):
    __tablename__ = 'Strategies'

    strategyID = Column(String(100), primary_key=True)
    description = Column(String(100), nullable=False)
    strategyParamsJson = Column(String(1000))
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class Agent(Base):
    __tablename__ = 'Agents'

    agentID = Column(String(100), primary_key=True)
    portfolioID = Column(ForeignKey('Portfolios.portfolioID'), nullable=False, index=True)
    strategyID = Column(ForeignKey('Strategies.strategyID'), nullable=False, index=True)
    agentName = Column(String(100))
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))

    Portfolio = relationship('Portfolio')
    Strategy = relationship('Strategy')


class Balance(Base):
    __tablename__ = 'Balances'

    balanceID = Column(String(100), primary_key=True)
    assetID = Column(ForeignKey('Assets.assetID'), nullable=False, index=True)
    portfolioID = Column(ForeignKey('Portfolios.portfolioID'), nullable=False, index=True)
    total = Column(Float, nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))

    Asset = relationship('Asset')
    Portfolio = relationship('Portfolio')


class Market(Base):
    __tablename__ = 'Markets'

    marketID = Column(String(100), primary_key=True)
    baseAssetID = Column(ForeignKey('Assets.assetID'), nullable=False, index=True)
    counterAssetID = Column(ForeignKey('Assets.assetID'), nullable=False, index=True)
    exchangeID = Column(ForeignKey('Exchanges.exchangeID'), nullable=False, index=True)

    Asset = relationship('Asset', primaryjoin='Market.baseAssetID == Asset.assetID')
    Asset1 = relationship('Asset', primaryjoin='Market.counterAssetID == Asset.assetID')
    Exchange = relationship('Exchange')


class Ratio(Base):
    __tablename__ = 'Ratios'

    ratioID = Column(VARCHAR(100), primary_key=True)
    assetID = Column(ForeignKey('Assets.assetID'), nullable=False, index=True)
    portfolioID = Column(ForeignKey('Portfolios.portfolioID'), nullable=False, index=True)
    currentRatio = Column(Float, nullable=False)
    allowedRatio = Column(Float, nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))

    Asset = relationship('Asset')
    Portfolio = relationship('Portfolio')


class Order(Base):
    __tablename__ = 'Orders'

    orderID = Column(String(100), primary_key=True)
    marketID = Column(ForeignKey('Markets.marketID'), nullable=False, index=True)
    parentOrderID = Column(ForeignKey('Orders.orderID'), index=True)
    amountBase = Column(Float, nullable=False)
    amountCounter = Column(Float, nullable=False)
    amountUSD = Column(Float, nullable=False)
    status = Column(String(100), nullable=False)
    type = Column(String(100), nullable=False)
    rate = Column(Float, nullable=False)
    timestamp = Column(INTEGER(11), nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    side = Column(String(100), nullable=False)

    Market = relationship('Market')
    parent = relationship('Order', remote_side=[orderID])

    
class Price(Base):
    __tablename__ = 'Ratios'

    ratioID = Column(VARCHAR(100), primary_key=True)
    assetID = Column(ForeignKey('Assets.assetID'), nullable=False, index=True)
    portfolioID = Column(ForeignKey('Portfolios.portfolioID'), nullable=False, index=True)
    currentRatio = Column(Float, nullable=False)
    allowedRatio = Column(Float, nullable=False)
    createdOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updatedOn = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))

    Asset = relationship('Asset')
    Portfolio = relationship('Portfolio')