"""
This is the "example" module.

The example module supplies one function, factorial().  For example,

>>> from setup_paths import setup_paths
All Ok!
"""
import sys
import os
import doctest


def test_setup_paths():
    """Set the paths required by the program. Variables are collected from the global evenironment
    >>> import setup_paths
    >>> test_setup_paths()
    All Ok!
    """
    import setup_paths
    print("All Ok!")
    

def main():
    doctest.testmod()
    test_setup_paths()
    
    
if __name__ == "__main__":
    main()

# class testAccounSetup(TestCase):
#     def 

# if __name__ == '__main__':
#     # trader = setupUpLiveEnviron()
#     SetUpSim = False
#     if SetUpSim:
#         trader = createSimulation(
#             portfolioName="Sim3",
#             strategyName="acrobat",
#             agentName="Sim3",
#             exchangeName="binanceSIM",
#         )
#     else:
#         trader = setupUpLiveEnviron()
