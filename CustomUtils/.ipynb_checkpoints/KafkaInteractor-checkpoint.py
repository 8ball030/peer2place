from .CONFIG import kafka
from pykafka import KafkaClient
from pykafka.common import OffsetType


def getConsumer(
        topic,
        group=str(1)):
    c = KafkaClient(hosts=kafka['hosts'])
    t = c.topics[topic]
    consumer = t.get_simple_consumer(consumer_group=topic + group,
                                     use_rdkafka=True,
                                     queued_max_messages=10**3,
                                     auto_commit_enable=True,
                                     auto_offset_reset=OffsetType.LATEST)
    return consumer


def getProducer(topic,
                group=str(1)):
    c = KafkaClient(hosts=kafka['hosts'],
                    # use_greenlets=True
                    )
    t = c.topics[topic]
    producer = t.get_producer(
        use_rdkafka=True,
        linger_ms=1)
    return producer
