
from .CONFIG import DATABASES as DATABASES

from opentsdb import TSDBClient

#tsdb = TSDBClient('opentsdb.address', static_tags={'node': 'ua.node.12'})
#
#tsdb.send('metric.test', 1, tag1='val1', tag2='val2')
#
#tsdb.close()
#tsdb.wait()



class DBInteractor:
    def __init__(self):
        raise NotYetImplemented
    def write(self, priceMessage):
        raise NotYetImplemented

    def read(self, query):
        raise NotYetImplemented
        
class openTSDB(DBInteractor):
    def __init__(self, topic=None):
        self.tsdb = TSDBClient(DATABASES["OpenTSDB"]["address"],
                          static_tags={'node': 'ua.node.12'})
        self.topic = topic
    
    def write(self, priceMessage):
        """
        parses and writes to db excpect pack in the form
        {"market":
        "timeStamp",
        "amount",
        "rate"}
        """
        print("Writing ", priceMessage)
        self.tsdb.send(self.topic, 1, 
                      tag1=priceMessage['market'],
                      tag2=priceMessage['timeStamp'],
                      tag3=priceMessage['amount'],
                      tag4=priceMessage['rate'],
                      )
        self.tsdb.close()
        self.tsdb.wait()
    
if __name__ == "__main__":
    db = openTSDB("test")
    db.write({"market": "ETH/BTC",
              "timeStamp": 100111020101,
              "amount": 10,
              "rate": 1.632})
    
