
import sys

from .CONFIG import DATABASES
from .dbModels.out import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def setUpSession(*, configSet):
    # print(f"config set : {configSet}")
    config = DATABASES[configSet]
    conn = 'postgresql://{}:{}@{}:{}/{}'.format(
        config['sql_username'],
        config['sql_password'],
        config['host'],
        config['port'],
        config["database"]
    )
    engine = create_engine(conn, pool_pre_ping=True)
    session = sessionmaker()
    session.configure(bind=engine)
    Base.metadata.create_all(engine)
    s = session()
    return s


def main():
    s = setUpSession()
    return s


if __name__ == '__main__':
    main()
