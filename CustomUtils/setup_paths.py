"""
This is the "example" module.

The example module supplies one function, factorial().  For example,

>>>setup_paths()
All Ok!
"""
import sys
import os
import doctest

global sys

def setup_paths():
    """Set the paths required by the program. Variables are collected from the global evenironment
    >>> setup_paths()
    All Ok!
    """
    try:
        sys.path += [
                     "/app/simulation_service/",
                     "/app/",
                    ]
        print("All Ok!")
    except:
        print("Error importing paths..")

# setup_paths()

if __name__ == "__main__":
    main()
