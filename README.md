# Peer2Place

A decentralised alternative to Uber.

## Architecture

### Skills
- Pathfinding 
Uses a rotating set of free API keys in order to connect to a free routing finding software

- Search 1
This skill retrieves the state from all registered Driver and Passenger agents in order to construct a state object to be passed to the front end 

- Search 1 response
Allows the agents to pass their state.

- Web Server
Produces a swagger documented API set in order to allow an entry point to instantiate Passengers agents and to retrieve state from all agents.

- Search 2
 Passenger search to retrieve Drivers

- Search 2 response
 Drivers response to the passengers
 
- Locamote Driver
Updates the position of the Driver in accordance with the path retrieved from the pathfinder. Additionally, passes message to Passenger to inform of new state.


- Contract Deployment, Payment and Escrow
This skill allows the passenger agents to set aside funds for the Journey and to release the escrow when the passenger has arrived.

 
### Agents

- Path Finder
Responsibility is to generate paths. We have chosen to implement as a separate Agent in order to allow the cycling of API keys for a free route finding service available online.

- Middle-ware 
Acts a controller agent to the allow the front end to retrieve states 

- Drivers

- Passengers

### Diagrams
This shows the primary Entities involved in the application.
The Front end is a React Web-app which communicates with the middle-ware agent. For Agent creation, the middle-ware agent will send a number of post requests to the AEA CLI GUI http server. 

#### Entities

![Test](./docs/architecture.png) 

##### Passenger Creation
![Test](./docs/agentCreation.png) 


#### Communication Flows

##### Main Loop
![Test](./docs/looping.png) 

##### Driver 2 Passenger Communications
![Test](./docs/processFlow.png) 






## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo







## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

