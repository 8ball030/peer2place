import React from 'react';
import { MDBIcon, MDBCard, MDBCardText } from 'mdbreact';

class InfoPanel extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.expanded) {
      return (false);
    }
    return (
        <MDBCard className="info-panel">
          <MDBIcon icon="times" onClick={console.log} className="help-close"/>
          <MDBCardText>
            <ul>
              {this.props.contracts.map(contract => <li>{contract.name}</li>)}
            </ul>
          </MDBCardText>
        </MDBCard>
    );
  }
}

export default InfoPanel;
