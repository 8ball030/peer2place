import React from 'react';
import { MDBIcon, MDBBtn, MDBCard, MDBCardText } from 'mdbreact';
import { connect } from "react-redux";
import { toggleHelp } from "../redux/actions";

const Help = ({ expanded, toggleHelp }) => (
    expanded ?
      <MDBCard className="help-text">
        <MDBIcon icon="times" onClick={toggleHelp} className="help-close"/>
        <MDBCardText>
          Welcome to the Fetch.ai decentralised taxi demo.
        </MDBCardText>
        <ul>
          <li>Click the screen to create a passenger</li>
          <li>Click the passenger to create a journey</li>
          <li>Click a taxi to see their upcoming journeys</li>
        </ul>
      </MDBCard>
    :
      <MDBBtn className="help-button" onClick={toggleHelp}>
        <MDBIcon icon="question" size="2x" className="white-text"/>
      </MDBBtn>
);

const mapStateToProps = state => {
  return { expanded: state.help.expanded };
};

export default connect(mapStateToProps, { toggleHelp })(Help);
