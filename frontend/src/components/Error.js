import React from 'react';
import { MDBContainer, MDBAlert } from 'mdbreact';

const Error = props => (
  <MDBContainer>
    <MDBAlert color="danger">Error creating passenger: {props.message.message}</MDBAlert>
  </MDBContainer>
);

export default Error;
