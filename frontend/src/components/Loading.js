import React from 'react';
import { MDBContainer, MDBAlert } from 'mdbreact';

const Loading = props => (
  <MDBContainer>
    <MDBAlert color="info">Creating Passenger</MDBAlert>
  </MDBContainer>
);

export default Loading;
