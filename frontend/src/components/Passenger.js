import React from 'react';
import { Circle, Polyline } from 'react-leaflet';
import { PASSENGER_OPTIONS } from "../constants";

const Passenger = props => (
  <div>
    <Circle center={props.position} {...PASSENGER_OPTIONS} />
    {props.destination ? <Circle center={props.destination} radius={2} /> : false}
    {props.geometry ? <Polyline positions={props.geometry} /> : false}
  </div>
);

export default Passenger;
