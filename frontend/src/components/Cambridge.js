import React from 'react';
import { connect } from "react-redux";
import { spawnPassenger, passengerCreated, createFailed } from "../redux/actions";
import { create } from "../requests";
import { Map, TileLayer } from 'react-leaflet';
import Driver from "./Driver";
import Passenger from "./Passenger";
import Error from "./Error";
import Loading from "./Loading";
import { MAP_OPTIONS } from "../constants";

class Cambridge extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSpawnPassenger = event => {
    const position = [event.latlng.lat, event.latlng.lng];
    this.props.spawnPassenger(position);
    if (this.props.step === 0) {
      create(
        {...this.props.creating, destination: position},
        this.props.passengerCreated,
        this.props.createFailed
      );
    }
  }

  render() {
    return (
      <Map className="flex-grow-1" {...MAP_OPTIONS} onClick={this.handleSpawnPassenger}>
        <TileLayer
          url='https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png'
          attribution="&copy; <a href=&quot;https://openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors &copy; <a href=&quot;https://carto.com/attributions&quot;>CARTO</a>"
        />
        {this.props.drivers.map( d => <Driver {...d} />)}
        {this.props.passengers.map( p => <Passenger {...p} />)}
        {this.props.creating.position ? <Passenger {...this.props.creating} /> : false}
        {this.props.error ? <Error message={this.props.error} /> : false}
        {this.props.isSending ? <Loading /> : false}
      </Map>
    );
  }
}

const mapStateToProps = state => {
  return state.map;
};

export default connect(mapStateToProps, { spawnPassenger, passengerCreated, createFailed })(Cambridge);
