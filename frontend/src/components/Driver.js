import React from 'react';
import { Circle } from 'react-leaflet';
import { DRIVER_OPTIONS } from "../constants";

const Driver = props => (
  <Circle center={props.position} {...DRIVER_OPTIONS} />
);

export default Driver;
