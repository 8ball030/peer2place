export const MAP_OPTIONS = {
  center: [52.2053, 0.1218],
  zoom: 17,
  minZoom: 15,
  maxZoom: 19,
  maxBounds: [[52.1953, 0.1018], [52.2153, 0.1418]]
};

export const DRIVER_OPTIONS = {
  radius: 4,
  color: '#14e360',
  fillOpacity: 0.6,
  bubblingMouseEvents: false
};

export const PASSENGER_OPTIONS = {
  radius: 4,
  color: '#ff0022',
  bubblingMouseEvents: false
};

export const API_URL = 'http://localhost:8080';
