import { TOGGLE_HELP, SPAWN_PASSENGER, PASSENGER_CREATED, CREATE_FAILED } from "./actionTypes";

export const toggleHelp = () => ({
  type: TOGGLE_HELP,
  payload: {}
});

export const spawnPassenger = position => ({
  type: SPAWN_PASSENGER,
  payload: { position }
});

export const passengerCreated = json => ({
  type: PASSENGER_CREATED,
  payload: json
});

export const createFailed = error => ({
  type: CREATE_FAILED,
  payload: error
});
