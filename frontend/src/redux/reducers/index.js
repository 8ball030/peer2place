import { combineReducers } from "redux";
import help from "./help";
import map from "./map";

export default combineReducers({ help, map });
