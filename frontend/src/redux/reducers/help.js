import { TOGGLE_HELP } from "../actionTypes";

const initialState = {
  expanded: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_HELP: {
      return {
        ...state,
        expanded: !state.expanded
      };
    }
    default:
      return state;
  }
}
