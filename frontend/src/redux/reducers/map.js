import { SPAWN_PASSENGER, PASSENGER_CREATED, CREATE_FAILED } from "../actionTypes";

const initialState = {
    drivers: [{position: [52.2053, 0.1218], key: 'driver1'}],
    passengers: [],
    creating: {},
    isSending: false,
    step: 0,
    error: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SPAWN_PASSENGER:
      if (state.isSending) {
        return state;
      }
      switch (state.step) {
        case 0:
          return {
            ...state,
            step: 1,
            creating: action.payload
          };
        case 1:
          return {
            ...state,
            step: 0,
            isSending: true,
            creating: {...state.creating, destination: action.payload.position}
          };
        default:
          return state;
      }
    case PASSENGER_CREATED:
      return {
        ...state,
        passengers: [...state.passengers, action.payload],
        step: 0,
        creating: {},
        isSending: false,
        error: false
      }
    case CREATE_FAILED:
     return {
       ...state,
       step: 0,
       isSending: false,
       creating: {},
       error: action.payload
     }
    default:
      return state;
  }
}
