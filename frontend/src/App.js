import React from 'react';
import { MDBNavbar, MDBNavbarBrand } from 'mdbreact';
import Help from "./components/Help";
import Cambridge from "./components/Cambridge";
import InfoPanel from "./components/InfoPanel";

class App extends React.Component {
  constructor() {
    super();
    this.apiKey = '5b3ce3597851110001cf62488d1d35fc53a54650bca6455336dfb87b';
    this.state = {
      lat: 52.2053,
      lng: 0.1218,
      bounds: [[52.1953, 0.1018], [52.2153, 0.1418]],
      drivers: [{lat: 52.2053, lng: 0.1218}],
      passengers: [],
      contracts: [],
      journeySelect: 0
    }
  }

  getRandomLocation() {
    return [this.getRandomLat(), this.getRandomLng()];
  }

  getRandomLat() {
    return this.randFromBounds(this.state.bounds[0][0], this.state.bounds[1][0]);
  }

  getRandomLng() {
    return this.randFromBounds(this.state.bounds[0][1], this.state.bounds[1][1]);
  }

  randFromBounds(lower, upper) {
    return lower + (Math.random() * (upper - lower));
  }

  getRandomRoute(callback, lat, lng) {
    fetch(
      'https://api.openrouteservice.org/directions?api_key=' +
        this.apiKey +
        '&coordinates=' +
        (lng || this.getRandomLng()) +
        ',' +
        (lat || this.getRandomLat()) +
        '|' +
        this.getRandomLng() +
        ',' +
        this.getRandomLat() +
        '&profile=driving-car&geometry=true&geometry_format=polyline'
    )
      .then(data => data.json())
      .then(jsonData => {
        callback(jsonData.routes[0].geometry)
      });
  }


  render() {
    return (
      <div className="app min-100 d-flex flex-column">
        <MDBNavbar variant="dark">
          <MDBNavbarBrand>
            <img
              alt="Fetch.ai"
              src="https://fetch.ai/wp-content/uploads/2019/10/logo-1.png"
              className="fetch-brand"
            />{' '}
            <span className="white-text">Decentralised Driver Demo</span>
          </MDBNavbarBrand>
        </MDBNavbar>
        <Cambridge lat={this.state.lat} lng={this.state.lng} bounds={this.state.bounds} drivers={this.state.drivers} passengers={this.state.passengers} />
        <Help />
        <InfoPanel expanded={false} contracts={[{name: 'test'}, {name: 'test2'}]} />
      </div>
    );
  }
}

export default App;
