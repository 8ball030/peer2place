import { API_URL } from "./constants";

export function create(data, success, error) {
  fetch (API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  }).then(function (response) {
    if (!response.ok) {
      throw new Error("HTTP error, status = " + response.status);
    }
    return response.json();
  }).then(
    success
  ).catch(
    error
  );
}
