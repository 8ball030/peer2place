from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from CustomUtils.KafkaInteractor import getProducer
from threading import Thread
import json
# https://flask-restplus.readthedocs.io/en/stable/example.html

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='MVC Agent Controller',
    description='A simple AgentMVC API',
)
ns = api.namespace('Agents', description='Agent operations')

from flask_cors import CORS
CORS(app)

producer = getProducer("agent_stream")

STATES = []

Passenger = api.model('Passenger', {
    'id': fields.String(readOnly=True,
                         description='The Passenger unique identifier'),
    'name': fields.String(required=True,
                          description='The Passengers Name'),
    'currentLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'currentLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'endLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'endLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'carStylePreference': fields.Float(required=True,
                         description='The Passenger unique identifier'),

    'driverCommunityScorePreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'costPreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'waitTimePreference': fields.Float(required=True,
                          description='The Passengers Name'),

})


Driver = api.model('Driver', {
    'id': fields.String(readOnly=True,
                         description='The Driver unique identifier'),

    'name': fields.String(required=True,
                          description='The Drivers Name'),

    'status': fields.String(required=True,
                          description='The Drivers Status'),

    'currentLocationLat': fields.Float(required=True,
                          description='The Drivers current Latitude'),

    'currentLocationLong': fields.Float(required=True,
                          description='The Driver current Longitude'),

    'homeLocationLat': fields.Float(required=True,
                          description='The Drivers home Latitude'),

    'homeLocationLong': fields.Float(required=True,
                          description='The Driver home Longitude'),

    'carStylePreference': fields.Float(required=True,
                         description='The Driver unique identifier'),

    'driverCommunityScorePreference': fields.Float(required=True,
                          description='The Drivers Average Community Score'),

    'costPreference': fields.Float(required=True,
                          description='The Drivers Difference From Market Rate'),

    'finalDestinationFromHomePreference': fields.Float(required=True,
                          description='The Drivers Final Route Preference'),


})



class PassengerDOA(object):
    def __init__(self, id, name, currentLocationLat, currentLocationLong, endLocationLat, endLocationLong,
                 carStylePreference, costPreference, driverCommunityScorePreference, waitTimePreference):
        self.id = id
        self.name = name
        self.currentLocationLat = currentLocationLat
        self.currentLocationLong = currentLocationLong
        self.endLocationLat = endLocationLat
        self.endLocationLong = endLocationLong
        self.carStylePreference = carStylePreference
        self.driverCommunityScorePreference = driverCommunityScorePreference
        self.costPreference = costPreference
        self.waitTimePreference = waitTimePreference
        producer.produce(bytes(self.to_dict(), "utf-8"))


    def to_dict(self):
        packet = {
            "type": "passenger",
            "id": self.id,
            "name": self.name,
            "currentLocationLat": self.currentLocationLat,
            "currentLocationLong": self.currentLocationLong,
            "endLocationLat": self.endLocationLat,
            "endLocationLong": self.endLocationLong,
            "carStylePreference": self.carStylePreference,
            "driverCommunityScorePreference": self.driverCommunityScorePreference,
            "costPreference": self.costPreference,
            "waitTimePreference": self.waitTimePreference,
        }
        return json.dumps(packet)


class DriverDOA(object):
    def __init__(self, id, name, status, currentLocationLat, currentLocationLong, homeLocationLat, homeLocationLong,
                 carStylePreference, costPreference, driverCommunityScorePreference, finalDestinationFromHomePreference):
        self.id = id
        self.name = name
        self.status = status
        self.currentLocationLat = currentLocationLat
        self.currentLocationLong = currentLocationLong
        self.homeLocationLat = homeLocationLat
        self.homeLocationLong = homeLocationLong
        self.carStylePreference = carStylePreference
        self.driverCommunityScorePreference = driverCommunityScorePreference
        self.costPreference = costPreference
        self.finalDestinationFromHomePreference = finalDestinationFromHomePreference
        producer.produce(bytes(self.to_dict(), "utf-8"))


    def to_dict(self):
        packet = {
            "type": "driver",
            "id": self.id,
            "name": self.name,
            "status": self.status,
            "currentLocationLat": self.currentLocationLat,
            "currentLocationLong": self.currentLocationLong,
            "homeLocationLat": self.homeLocationLat,
            "homeLocationLong": self.homeLocationLong,
            "carStylePreference": self.carStylePreference,
            "driverCommunityScorePreference": self.driverCommunityScorePreference,
            "costPreference": self.costPreference,
            "finalDestinationFromHomePreference": self.finalDestinationFromHomePreference,
        }
        return json.dumps(packet)



@ns.route('/')
class passenger_list(Resource):
    '''Shows a list of all agents, and lets you POST to add new agents'''

    def get(self):
        '''List all agents'''
        return STATES

    @ns.doc('create_passenger')
    @ns.expect(Passenger)
    def post(self):
        '''Create a new passenger'''
        return PassengerDOA(**api.payload).to_dict(), 201


def main():
    p = PassengerDOA(id="testId",
                     name="Test",
                     currentLocationLat=0.01,
                     currentLocationLong=0.01,
                     endLocationLat=10,
                     endLocationLong=10,
                     carStylePreference=5,
                     driverCommunityScorePreference=5,
                     costPreference=10,
                     waitTimePreference=10,)

    d = DriverDOA(id="testIdD",
                  name="Test",
                  status="ready",
                  currentLocationLat=0.01,
                  currentLocationLong=0.01,
                  homeLocationLat=10,
                  homeLocationLong=10,
                  carStylePreference=5,
                  driverCommunityScorePreference=5,
                  costPreference=10,
                  finalDestinationFromHomePreference=10,
                  )

    STATES.append(p.to_dict())
    STATES.append(d.to_dict())


if __name__ == '__main__':
    main()
    app.run(debug=True,
            host="0.0.0.0",
            port=8082)
