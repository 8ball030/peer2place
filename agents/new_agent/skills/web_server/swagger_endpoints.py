from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from threading import Thread
# https://flask-restplus.readthedocs.io/en/stable/example.html

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='MVC Agent Controller',
    description='A simple PassengerMVC API',
)
ns = api.namespace('Passengers', description='Passenger operations')

from flask_cors import CORS
CORS(app)


STATES = []

Passenger = api.model('Passenger', {
    'id': fields.String(readOnly=True,
                         description='The Passenger unique identifier'),
    'name': fields.String(required=True,
                          description='The Passengers Name'),
    'currentLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'currentLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'endLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'endLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'carStylePreference': fields.Float(required=True,
                         description='The Passenger unique identifier'),

    'driverCommunityScorePreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'costPreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'waitTimePreference': fields.Float(required=True,
                          description='The Passengers Name'),

})

class PassengerDOA(object):
    def __init__(self, name, currentLocationLat, currentLocationLong, endLocationLat, endLocationLong,
                 carStylePreference, costPreference, driverCommunityScorePreference, waitTimePreference):
        self.name=name,
        self.currentLocationLat=currentLocationLat,
        self.currentLocationLong=currentLocationLong,
        self.endLocationLat=endLocationLat,
        self.endLocationLong=endLocationLong,
        self.carStylePreference=carStylePreference,
        self.driverCommunityScorePreference=driverCommunityScorePreference,
        self.costPreference=costPreference,
        self.waitTimePreference=waitTimePreference,

#@api.route('/')
#class get_passengers(Resource):
#    def get(self, **kwargs):
#        return STATES
#
#    @ns.doc('create_passenger')
#    @ns.expect(Passenger)
#    @ns.marshal_with(Passenger, code=201)
#    def post(self):
#        '''Create a new Passenger'''
#        return PassengerDOA(api.payload), 201

@ns.route('/')
class passenger_list(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''
    #@ns.doc('list_todos')
    #@ns.marshal_list_with(Passenger)
    def get(self):
        '''List all tasks'''
        return STATES

    @ns.doc('create_passenger')
    @ns.expect(Passenger)
    # @ns.marshal_with(Passenger, code=201)
    def post(self):
        '''Create a new task'''
        return PassengerDOA(**api.payload), 201


def main():
    p = PassengerDOA(name="Test",
                  currentLocationLat=0.01,
                  currentLocationLong=0.01,
                  endLocationLat=10,
                  endLocationLong=10,
                  carStylePreference=5,
                  driverCommunityScorePreference=5,
                  costPreference=10,
                  waitTimePreference=10,)

    STATES.append(p)



if __name__ == '__main__':
    main()
    app.run(debug=True)
