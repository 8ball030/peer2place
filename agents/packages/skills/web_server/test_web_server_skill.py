import pytest
from packages.skills.web_server import swagger_endpoints


@pytest.fixture
def app():
    app = create_app()
    return app
