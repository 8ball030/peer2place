# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#
#   Copyright 2018-2019 Fetch.AI Limited
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# ------------------------------------------------------------------------------

"""This module contains the behaviours for the 'webserver' skill."""

import logging
import sys
from multiprocessing import Process

from aea.skills.base import Behaviour

sys.path += ["/home/tom/Desktop/fetchAI/peerToPlace/agents"]
from packages.skills.web_server.swagger_endpoints import app


logger = logging.getLogger("aea.webserver_skill")


class WebServerBehaviour(Behaviour):
    """WebServer behaviour."""


    def __init__(self, **kwargs):
        """Initialize the webserver behaviour."""
        super().__init__(**kwargs)
        logger.info("WebServerBehaviour.__init__: arguments: {}".format(kwargs))
        self.server = Process(target=app.run)

    def setup(self) -> None:
        """Set up the behaviour."""
        self.server.start()
        logger.info("WebServer Behaviour: setup method called.")

    def act(self) -> None:
        """Act according to the behaviour."""
        logger.info("WebServer Behaviour: act method called.")

    def teardown(self) -> None:
        """Teardown the behaviour."""
        logger.info("WebServer Behaviour: teardown method called.")
