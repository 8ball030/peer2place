#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 01:30:21 2019

@author: tom
"""
import json
from uuid import uuid4
from datetime import datetime
from multiprocessing import Process
import subprocess

from CustomUtils.KafkaInteractor import getConsumer


def setup(topic):
    consumer = getConsumer(topic)
    return consumer

def spawnDriverAgent(params):
    print("here!", type(params), params)

    subprocess.Popen([f"./makeDriver.sh", "driver_agent,sender_agent,default,"+ str(params)])


def mainLoop():
    topic = 'agent_stream'
    consumer = setup(topic)
    print("Starting to consume agent specifications")
    while True:
        msg = consumer.consume()
        msg = json.loads(msg.value)
        msg["id"] = str(uuid4())
        msg["timeStamp"] = datetime.now().isoformat()
        print("Spawing agent with parameters:", msg)
        p = Process(target=spawnDriverAgent, args=[msg])
        p.start()
        print("Spawned! \o/")


if __name__ == '__main__':
    mainLoop()
    print("done")

