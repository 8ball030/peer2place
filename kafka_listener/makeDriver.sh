#! /bin/bash
echo "Creating Driver"
rm -r driver_agent
pipenv run aea create driver_agent
cd driver_agent
echo $1 >> input_file
pipenv run aea add skill driver_broadcast
pipenv run aea add skill consume_startup_params
pipenv run aea add connection stub
pipenv run aea run --connections stub
